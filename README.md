# What is voided-packages?

voided-packages is a collection of unofficially maintained XBPS packages that doesn't meet the requirements to be included in the void-packages repository.

## Compiling
### Preparing the environment

Clone the void-packages repository:
```
$ git clone https://github.com/void-linux/void-packages
```

cd into cloned `void-packages` repository and install the bootstrap
packages:
```
$ cd void-packages
$ ./xbps-src binary-bootstrap
```

### Clone voided-packages repository
```
$ git clone https://codeberg.org/saeweard/voided-packages.git
```
Once you have cloned the repository, copy the contents of srcpkgs folder
into the void-packages srcpkgs folder.

### Building the package
Once the bootstrap is ready, you can build the package with:
```
$ ./xbps-src pkg <pkgname>
```
**Note**: _if your computer has enough resources, the -j \<n\> option can be used to
speed the building process, just take into consideration that 6 GiB of memory
is needed per job to prevent the system from hanging._

After a while, the above step will create a binary in `hostdir/binpkgs` named
`<pkgname>-<version>_<revision>.<arch>.xbps`.

### Building for another architecture
You must first create a new masterdir for the desired architecture:
```
$ ./xbps-src -m <masterdir> binary-bootstrap <arch>
```
where `<masterdir>` can have any name, I suggest `masterdir-<arch>`.

Once the bootstrap for the new masterdir is ready, you can proceed to build
packages with:
```
$ ./xbps-src -m <masterdir> pkg <package>
```

## Installing
Once the package is in `hostdir/binpkgs` you can install it with:
```
# xbps-install -R hostdir/binpkgs <package>
```
